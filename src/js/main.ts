import * as Turbolinks from 'turbolinks';
import { Application } from "stimulus"
import * as controllers from "./controllers/";

// start Turbolink Application
Turbolinks.start();

// start Stimulus application
const application = Application.start();

// register all controllers in controllers/index.ts 
for (let controller in controllers) {
  application.register(controllers[controller].name, controllers[controller].controller);
}