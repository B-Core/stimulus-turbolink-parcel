import { Controller } from "stimulus"

class Counter extends Controller {
  connect() {
    console.log("Hello, Stimulus!", this.element)
  }
  greet() {
    console.log(`Hello, ${this.name}!`);
  }

  get name() {
    return this.targets.find('name').value;
  }
}

export default ({
  name: "Counter",
  controller: Counter,
});